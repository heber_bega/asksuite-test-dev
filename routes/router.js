const express = require('express');
const router = express.Router();
const browserService = require('../services/BrowserService')
const scraperService = require('../services/ScraperService')


router.get('/', (req, res) => {
    res.send('Hello Asksuite World!');
});

router.post('/search', async (req, res) => {

    if(typeof(req.body) === 'object'){
        if(typeof(req.body.checkin) === 'string'){
            checkin = req.body.checkin
            checkout = req.body.checkout

            dataCheckin = checkin.split('-')[2] + checkin.split('-')[1] + checkin.split('-')[0] 
            dataCheckout = checkout.split('-')[2] + checkout.split('-')[1] + checkout.split('-')[0] 
            
            url = 
            "https://book.omnibees.com/hotelresults?CheckIn=" + dataCheckin + "&CheckOut=" + dataCheckout + "&Code=AMIGODODANIEL&NRooms=1&_askSI=d34b1c89-78d2-45f3-81ac-4af2c3edb220&ad=2&ag=-&c=2983&ch=0&diff=false&group_code=&lang=pt-BR&loyality_card=&utm_source=asksuite&q=5462#show-more-hotel-button";
        
            browser = await browserService.getBrowser();
            page = await browserService.getPage(browser);
            await browserService.goToUrl(page, url);

            roomData = await scraperService.getRoomsData(page)
            if(roomData.length > 0) {
                res.send(roomData)
            } else {
                res.status(404).send({errors: [{message: "Rooms not available for this date."}]})
            }

            await browserService.closeBrowser(browser);
        } else {
            res.status(400).send({errors: [{message: "Expected body in JSON format: \n{'checkin': 'AAAA-MM-DD', 'checkout': 'AAAA-MM-DD'}"}]})
        }
    }
});

module.exports = router;
