const puppeteer = require('puppeteer');
// Module that controls the puppeteer browsing functions
class BrowserService {

    // returns a new browser
    static async getBrowser() {
        // return await puppeteer.launch({ headless: false }); // default is true
        return await puppeteer.launch({});
    }

    // returns a new page
    static async getPage(browser) {
        return await browser.newPage();
    }

    // closes a open browser instance
    static closeBrowser(browser) {
        if (!browser) {
            return;
        }
        return browser.close();
    }

    // receives a browser page and redirects it to the incomming url and does not timeout
    static async goToUrl(page, url){
        await page.goto(url, {
            waitUntil: 'load',
            // Remove the timeout
            timeout: 0
        });
    }
}

module.exports = BrowserService;
