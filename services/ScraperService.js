const puppeteer = require('puppeteer');

// Module that controls the web scrapping functionality
class ScraperService {

    // receives a page with rooms data and retrieve the data from the html document
    static async getRoomsData(page){

      await page.waitForSelector('#hotels_grid')

      // evaluates the rooms div container div
      return await page.$eval(
        '#hotels_grid',
        (element) => {
          filtered_children = Array.from(element.children)

          // interates through the rooms array retrieving data
          rooms_array = filtered_children.map((el) => {

            nameQuery = el.querySelector("div.flex-view-step2 > div.desciption.position-relative > span > p")
            descriptionQuery = el.querySelector(".description_mobile")
            priceQuery = el.querySelector("div.right-part-of-rate > div > div.price-step2.t-tip__next > p.price-total")
            imageQuery = el.querySelector("div.flex-view-step2 > div.t-tip__next > div > img.image-step2")

            if(priceQuery) {
              obj = {};
  
              obj.name = nameQuery ? nameQuery.innerText  : null;
              obj.description = descriptionQuery ? descriptionQuery.innerText  : null;
              obj.price = priceQuery ? priceQuery.innerText  : null;
              obj.image = imageQuery ? imageQuery.src  : null;
              
              return obj
            } else {
              return null
            }
          }).filter((el) => el) // removes rooms that arent available

          return rooms_array
        }
      )
    }
}

module.exports = ScraperService;
